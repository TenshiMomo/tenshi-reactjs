"use strict";
const path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const PROD = "prod";

module.exports = function(env) {
  const getScssLoader = () => {
    return {
      test: /\.scss$/,
      use: [
        MiniCssExtractPlugin.loader,
        {
          loader: "css-loader",
          options: {
            modules: true,
            sourceMap: env !== PROD && true,
            minimize: env === PROD && true
          }
        },
        {
          loader: "sass-loader"
        },
        {
          loader: "postcss-loader"
        }
      ]
    };
  };

  return {
    devServer: {
      port: 3000,
      compress: env === PROD,
      historyApiFallback: true,
    },
    mode: env === PROD ? "production" : "development",
    entry: [path.join(__dirname, "/src/")],
    output: {
      path: path.join(__dirname, "dist/static"),
      filename: "bundle.js",
      publicPath: "/static/"
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: "app.css",
        chunkFilename: "[name].css"
      }),
    ],
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          include: [
            path.join(__dirname, "/src/"),
            path.join(__dirname, "/library/")
          ],
          use: ["babel-loader"]
        },
        getScssLoader()
      ]
    },
    resolve: {
      alias: {
        library: path.resolve(__dirname, "library/"),
        pages: path.resolve(__dirname, "src/components/pages")
      }
    },
    watch: true
  };
};
