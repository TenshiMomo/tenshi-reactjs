# Tenshi framework with ReactJs / sass / hot-reloading

The minimal dev environment to enable live-editing React components with sass.

### Usage

```
git clone git@bitbucket.org:TenshiMomo/tenshi-reactjs.git
yarn (install node_modules)
yarn start
open http://localhost:3000

gulp build for production distribution
```
