import React, { Component } from "react";
import "./heading.scss";

class Heading extends Component {
  render() {
    const { title } = this.props;
    return <h1 className="heading">{title}</h1>;
  }
}

export default Heading;
