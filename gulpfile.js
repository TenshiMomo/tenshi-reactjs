const gulp = require("gulp");
const gulpsync = require("gulp-sync")(gulp);
const env = require("gulp-env");
const gutil = require("gulp-util");
const clean = require("gulp-clean");

const webpack = require("webpack");
const WebpackDevServer = require("webpack-dev-server");
const webpackConfigGetter = require("./webpack.config.getter");

// const sass = require('gulp-sass');
// const prefix = require('gulp-autoprefixer');
// const minifycss = require('gulp-minify-css');

var paths = {
  publicJsPath: "/static/",
  jsSources: "src/**/*"
};

gulp.task("clean-build", function() {
  return gulp.src("dist", { read: false }).pipe(clean());
});

/*** DEVELOPMENT BUILD ***/
// gulp.task('dev', sync('set-dev-env', 'webpack:dev-server'), function() {
//   gulp.watch([paths.jsSources], ['webpack:build-dev']);
// });
//
// gulp.task('webpack:dev-server', function(callback) {
//   // modify some webpack config options
//   const config = webpackConfigGetter();
//   console.log(config);
//
//   // Start a webpack-dev-server
//   new WebpackDevServer(webpack(config), {
//     publicPath: paths.publicJsPath,
//     filename: 'bundle.js',
//     stats: {
//       colors: true
//     }
//   }).listen(8080, 'localhost', function(err) {
//     if(err) throw new gutil.PluginError('webpack:dev-server', err);
//     gutil.log('[webpack-dev-server]', 'http://localhost:8080');
//   });
// });
//
// gulp.task('set-dev-env', function() {
//   setEnv('dev');
// });

/*** PRODUCTION BUILD ***/
gulp.task(
  "build",
  sync("clean-build", "set-prod-env", "webpack:build", "build-files")
);

gulp.task("webpack:build", ["set-prod-env"], function(callback) {
  // run webpack
  webpack(webpackConfigGetter("prod"), function(err, stats) {
    if (err) throw new gutil.PluginError("webpack:build", err);
    gutil.log("[webpack:build]", stats.toString({ colors: true }));
    callback();
  });
});

gulp.task("set-prod-env", function() {
  setEnv("PROD");
});

gulp.task("build-files", [], function() {
  gulp.src(["index.html"]).pipe(gulp.dest("dist"));
});

/*** HELPER FUNCTIONS ***/

function setEnv(buildEnv) {
  env({
    vars: {
      BUILD_ENV: buildEnv
    }
  });
}

function sync() {
  return gulpsync.sync([].slice.call(arguments));
}
