import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "pages/Home";
import Contact from "pages/Contact";

export default (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/contact" component={Contact} />
  </Switch>
);
