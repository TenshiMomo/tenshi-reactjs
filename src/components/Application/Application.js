import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import Pages from "library/common/enum/Pages";
import Heading from "library/components/atoms/Heading";
import Routes from "./../Routes";

class Application extends Component {
  render() {
    return (
      <Fragment>
        <Heading title="Welcome to Tenshi framework with Reactjs" />
        <ul>
          <li>
            <Link to={Pages.HOME}>Home</Link>
          </li>
          <li>
            <Link to={Pages.CONTACT}>Contact</Link>
          </li>
        </ul>
        {Routes}
      </Fragment>
    );
  }
}

export default Application;
